// @flow

import type {Game, Player, Standing} from '../lib/types';

import {generateStandings} from '../lib/functions/standings';

export default class Team {
  name: string;
  games: Array<Game> = []
  squad: Set<Object> = new Set()
  manager: string = ''
  standing: Standing = {}

  constructor(name: string) {
    this.name = name;
  }

  get name() {
    return this.name;
  }

  set name(name: string) {
    this.name = name;
  }

  get games() {
    return this.games;
  }
  
  set games(games: Game) {
    this.games = games;
  }

  get squad() {
    return Array.from(this.squad);
  }

  addTosquad(player: Player) {
    this.squad.add(player);
  }

  setStanding() {
    if(this.games.length > 0) {
      this.standing = generateStandings(this.name, this.games);
    } else {
      throw new Error(`[DEV ERROR] No games available in ${this.name} object. 
        Add games to this class before generating standings`);
    }
  }
}
