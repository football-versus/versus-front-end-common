import {getSeasons} from '../../actions/dates.action';

export default function(store) {
  store.dispatch(getSeasons());
}
