export type Player = {
  "lineup_player": string,
  "lineup_number": string,
  "lineup_position": string
}
