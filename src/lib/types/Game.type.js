// @flow
import type {Player} from './Player.type';

export type Game = {
  "match_id": string,
  "country_id": string,
  "country_name": string,
  "league_id": string,
  "league_name": string,
  "match_date": string,
  "match_status": string,
  "match_time": string,
  "match_hometeam_name": string,
  "match_hometeam_score": string,
  "match_awayteam_name": string,
  "match_awayteam_score": string,
  "match_hometeam_halftime_score": string,
  "match_awayteam_halftime_score": string,
  "match_hometeam_extra_score": string,
  "match_awayteam_extra_score": string,
  "match_hometeam_penalty_score": string,
  "match_awayteam_penalty_score": string,
  "match_hometeam_system": string,
  "match_awayteam_system": string,
  "match_live": string,
  "goalscorer": Array<string>,
  "cards": Array<string>,
  "lineup": {
    "home": {
      "starting_lineups": Array<Player>,
      "substitutes": Array<Player>,
      "coach": Array<string>,
      "substitutions": Array<string>
    },
    "away": {
      "starting_lineups": Array<Player>,
      "substitutes": Array<Player>,
      "coach": Array<string>,
      "substitutions": Array<string>
    }
  },
  "statistics": Array<string>
}
