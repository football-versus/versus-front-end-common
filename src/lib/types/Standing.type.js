// @flow

export type Standing = {
  points: Array<number>,
  pointsCumulative: Array<number>,
  form: Array<string>,
  gamesTotal: {
    played: number,  
    won: number,
    drawn: number,
    lost: number
  },
  goals: {
    for: number,
    against: number,
    difference: number
  }
}
