import type {Game} from './Game.type';
import type {Player} from './Player.type';
import type {Standing} from './Standing.type';

export default {
  Game, 
  Standing,
  Player
};
