// @Flow

import endpoints from '../lib/api/endpoints.js';
import fp from 'lodash/fp';
import {getCountries} from '../actions/countries.action';
import {getLeagues} from '../actions/leagues.action';
import {networkRequest} from '../lib/network';

export default {
  '[COUNTRIES] GET_COUNTRIES__SUBMIT': async (store, next, action) => {

    const _endpoint = endpoints['GET_COUNTRIES'];

    try {
      const res = await networkRequest(_endpoint, fp.get('payload', action));
      store.dispatch(getCountries.resolved(res.data));

    } catch (error) {
      console.error(`[ERROR] get countries middleware\n`, error);
      store.dispatch(getCountries.rejected(JSON.stringify(error)));
    }
  },

  '[COUNTRIES] SET_SELECTED_COUNTRY': async (store, next, action) => {
    const _countryId = {
      country_id: store.getState()
        .countriesReducer.selected.country_id
    };
    
    store.dispatch(getLeagues.submit(_countryId));
  },
};
