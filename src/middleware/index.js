import countriesMiddleware from './countries.middleware';
import leaguesMiddleware from './leagues.middleware';
import datesMiddleware from './dates.middleware';
import gamesMiddleware from './games.middleware';

const middlewares = {
  ...countriesMiddleware,
  ...leaguesMiddleware,
  ...datesMiddleware,
  ...gamesMiddleware
};

export default (store, next, action) => {
  const middlewareMode = action.payload
    ? action.payload.middlewareMode
    : false;
  
  if(!middlewareMode || middlewareMode === "first") {
    next(action);
  }

  const middleware = middlewares[action.type];

  if (middleware) {
    try {
      middleware(store, next, action);
    } catch (e) {
      throw new Error(`[VERSUS] ${e}`);
    }
  }

  if(middlewareMode === "last") {
    next(action);
  }
};
