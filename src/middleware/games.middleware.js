import endpoints from '../lib/api/endpoints.js';
import fp from 'lodash/fp';
import {getGames} from '../actions/games.action';
// @Flow
import {networkRequest} from '../lib/network';

export default {
  '[GAMES] GET_GAMES__SUBMIT': async (store, next, action) => {

    const _endpoint = endpoints['GET_GAMES'];

    try {
      const res = await networkRequest(_endpoint, fp.get('payload.req', action));
      store.dispatch(getGames.resolved(res.data));

    } catch (error) {
      console.error(`[ERROR] get games middleware\n`, error);
      store.dispatch(getGames.rejected(JSON.stringify(error)));
    }
  }
};
