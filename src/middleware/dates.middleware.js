// @Flow
import {setSeasons} from '../actions/dates.action';
import {getGames} from '../actions/games.action';

export default {
  '[DATES] GET_SEASONS': async (store, next, action) => {
    const seasons = await require('../data/season-dates.json');

    store.dispatch(setSeasons(seasons));
  },

  '[DATES] SET_DATE_SELECTION': async (store, next, action) => {
    const {start, end} = action.payload.dates;

    const _dates = {
      'from': `${start.year}-${start.month}-${start.day}`,
      'to': `${end.year}-${end.month}-${end.day}`,
    };

    action.payload = _dates;

    const league_id = store.getState().leaguesReducer.selected.league_id;

    store.dispatch(getGames.submit({
      req: {..._dates, league_id},
      middlewareMode: "last"
    }));
  },
};
