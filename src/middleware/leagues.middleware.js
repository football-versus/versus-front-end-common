// @Flow

import {getLeagueStandings, getLeagues} from '../actions/leagues.action';

import endpoints from '../lib/api/endpoints.js';
import fp from 'lodash/fp';
// import {getGames} from '../actions/games.action';
import {networkRequest} from '../lib/network';

export default {
  '[LEAGUES] GET_LEAGUES__SUBMIT': async (store, next, action) => {

    const _endpoint = endpoints['GET_LEAGUES'];

    try {
      const res = await networkRequest(_endpoint, fp.get('payload', action));
      store.dispatch(getLeagues.resolved(res.data));

    } catch (e) {
      console.error(`[ERROR] get leagues middleware\n`, e);
      store.dispatch(getLeagues.rejected(JSON.stringify(e)));
    }
  },

  '[LEAGUES] SET_SELECTED_LEAGUE': async (store, next, action) => {
    const _leagueId = store.getState().leaguesReducer.selected.league_id;
    // store.dispatch(getGames.submit(_leagueId));
  },

  '[LEAGUES] GET_LEAGUE_STANDINGS__SUBMIT': async (store, next, action) => {

    const _endpoint = endpoints['GET_LEAGUE_STANDINGS'];

    try {
      const res = await networkRequest(_endpoint, fp.get('payload', action));
      store.dispatch(getLeagueStandings.resolved(res.data));

    } catch (error) {
      console.error(`[ERROR] get leagues middleware\n`, error);
      store.dispatch(getLeagueStandings.rejected(JSON.stringify(error)));
    }
  }
};
