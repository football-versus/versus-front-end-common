// jest.config.js
module.exports = {
  verbose: true,
  collectCoverage: false,
  coverageDirectory: "./test/coverage",
  coverageReporters: ["text"],
  testPathIgnorePatterns: ["<rootDir>/src/", "node_modules"],
  coverageThreshold: {
    "global": {
      "branches": 0,
      "functions": 75,
      "lines": 75,
      "statements": 75
    }
  }
};