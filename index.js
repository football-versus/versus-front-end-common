import reducers from './src/reducers';
import actions from './src/actions';
import middleware from './src/middleware';
import utils from './src/lib/utils';

export default {
  reducers,
  actions,
  middleware,
  utils
};