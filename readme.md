# Versus [COMMON]


[![Total alerts](https://img.shields.io/lgtm/alerts/b/football-versus/versus-front-end-common.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/b/football-versus/versus-front-end-common/alerts/)
[![Language grade: JavaScript](https://img.shields.io/lgtm/grade/javascript/b/football-versus/versus-front-end-common.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/b/football-versus/versus-front-end-common/context:javascript)

## Develop

All code is done in the `./src` in es6

Everything should be **COVERED** in tests. This project does not have a client whcih we can use to test - therefore everything must be done programmaticaly through tests. 

Linting TBC

## Build

Run `npm run build` to compile the `src` (*es6*) folder into `./build` (*es5*)

Only the `src/lib`, `src/classes` and `src/data` are compiled into `./build`. This is because the actions, middleware and reducers and straight imported in the client projects which will inturn compile the es6 with webpack. This may change depending on test... TBC

## Test

Jest is the test runner and looks for the `.test.js` files in `./build`

This means we can use es6 syntax like imports in our tests. 

To test run `npm run test` or `npm run test:watch` when developing.

The build process will run before each test run ensuring the latest copy of the src is being tested.

## Deploy

This will published to `NPM` as a private repo - TBC